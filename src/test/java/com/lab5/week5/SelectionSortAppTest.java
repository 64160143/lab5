package com.lab5.week5;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SelectionSortAppTest {
    @Test
    public void shoudfindminindextest1(){
        int arr[] = {5,4,3,2,1};
        int pos = 0;
        int minindex = SelectionSortApp.Findminindex(arr,pos);
        assertEquals(4, minindex);

    }   

    @Test
    public void shoudfindminindextest2(){
        int arr[] = {1,4,3,2,5};
        int pos = 1;
        int minindex = SelectionSortApp.Findminindex(arr,pos);
        assertEquals(3, minindex);

    }
    @Test
    public void shoudfindminindextest3(){
        int arr[] = {1,2,3,4,5};
        int pos = 2;
        int minindex = SelectionSortApp.Findminindex(arr,pos);
        assertEquals(2, minindex);

    }   
    @Test
    public void shoudfindminindextest4(){
        int arr[] = {1,1,1,1,1,0,1,1};
        int pos = 0;
        int minindex = SelectionSortApp.Findminindex(arr,pos);
        assertEquals(5, minindex);

    } 
    @Test
    public void shoudSwaptest1(){
        int arr[] = {5,4,3,2,1};
        int expec[] = {1,4,3,2,5};
        int fir = 0;
        int sec = 4;
        SelectionSortApp.swap(arr,fir,sec);
        assertArrayEquals(expec,arr);

    } 
    @Test
    public void shoudSwaptest2(){
        int arr[] = {5,4,3,2,1};
        int expec[] = {5,4,3,2,1};
        int fir = 0;
        int sec = 0;
        SelectionSortApp.swap(arr,fir,sec);
        assertArrayEquals(expec,arr);

    }   
    @Test
    public void shouldSeclectionsortTestCase1(){
        int arr[] = {5,4,3,2,1};
        int sortedArr[] = {1,2,3,4,5};
        SelectionSortApp.selectonSort(arr);
        assertArrayEquals(sortedArr, arr);
    } 
    @Test
    public void shouldSeclectionsortTestCase2(){
        int arr[] = {10,9,8,7,6,5,4,3,2,1};
        int sortedArr[] = {1,2,3,4,5,6,7,8,9,10};
        SelectionSortApp.selectonSort(arr);
        assertArrayEquals(sortedArr, arr);
    }
    @Test
    public void shouldSeclectionsortTestCase3(){
        int arr[] = {10,9,8,7,6,5,4,3,2,1};
        int sortedArr[] = {1,2,3,4,5,6,7,8,9,10};
        SelectionSortApp.selectonSort(arr);
        assertArrayEquals(sortedArr, arr);
    }                          

    
}
