package com.lab5.week5;
import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;
public class BubbleSortAppTest {
    @Test
    public void ShouldBubbleTest1(){
        int arr[] = {5,4,3,2,1};
        int expec[] = {4,3,2,1,5};
        int fir = 0;
        int sec = 4;
        BubbleSortApp.bubble(arr,fir,sec);
        assertArrayEquals(expec, arr);
    }
    @Test
    public void ShouldBubbleTest2(){
        int arr[] = {4,3,2,1,5};
        int expec[] = {3,2,1,4,5};
        int fir = 0;
        int sec = 3;
        BubbleSortApp.bubble(arr,fir,sec);
        assertArrayEquals(expec, arr);
    }
    @Test
    public void ShouldBubbleTest3(){
        int arr[] = {3,2,1,4,5};
        int expec[] = {2,1,3,4,5};
        int fir = 0;
        int sec = 2;
        BubbleSortApp.bubble(arr,fir,sec);
        assertArrayEquals(expec, arr);
    }
    @Test
    public void ShouldBubbleTest4(){
        int arr[] = {2,1,3,4,5};
        int expec[] = {1,2,3,4,5};
        int fir = 0;
        int sec = 1;
        BubbleSortApp.bubble(arr,fir,sec);
        assertArrayEquals(expec, arr);
    }
    @Test
    public void Shouldbubblesorttestcase1(){
        int arr[] = {5,4,3,2,1};
        int expec[] = {4,3,2,1,5};
        BubbleSortApp.bubbleSort(arr);
        assertArrayEquals(expec, arr);
    }
    @Test
    public void Shouldbubblesorttestcase2(){
        int arr[] = {10,9,8,7,6,5,4,3,2,1};
        int sortedArr[] = {1,2,3,4,5,6,7,8,9,10};
        BubbleSortApp.bubbleSort(arr);
        assertArrayEquals(sortedArr, arr);
    }
    @Test
    public void Shouldbubblesorttestcase3(){
        int arr[] = {10,9,8,7,6,5,4,3,2,1};
        int sortedArr[] = {1,2,3,4,5,6,7,8,9,10};
        BubbleSortApp.bubbleSort(arr);
        assertArrayEquals(sortedArr, arr);
    }     


    
}



