package com.lab5.week5;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void shouldAdd1And1Is2(){
        int re = App.add(1,1);
        assertEquals(2, re);
    }
    @Test
    public void shouldAdd1And1Is3(){
        int re = App.add(2,1);
        assertEquals(3, re);
    }
    @Test
    public void shouldAdd1And0IsMin1(){
        int re = App.add(-1,0);
        assertEquals(-1, re);
    }    

}
