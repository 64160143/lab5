package com.lab5.week5;

public class SelectionSortApp {
    public static void main(String[] args) {
        int arr[] = new int[1000];
        randomArray(arr);
        print(arr);
        selectonSort(arr);
        print(arr);
        
        
    }
    public static void print(int arr[]){
        for(int a:arr){
            System.out.println(a + " ");
        }
        System.out.println();
    }
    
    public static void randomArray(int arr[]){
        for(int i = 0; i<arr.length;i++){
            arr[i] = (int)(Math.random()*10000);
        } 
    }

    public static int Findminindex(int[] arr, int pos){
        int minindex = pos;
        for(int i = pos+1 ; i<arr.length;i++){
            if (arr[minindex]>arr[i]){
                minindex = i;
            }
        }
        return minindex;
    

    }
    public static void swap(int[] arr, int fir, int sec) {
        int temp = arr[fir];
        arr[fir] = arr[sec];
        arr[sec] = temp;

    }
    public static void selectonSort(int[] arr) {
        int minindex = 0;
        for(int pos = 0; pos<arr.length-1;pos++){
            minindex = Findminindex(arr,pos);
            swap(arr, minindex ,pos);
        }

    }
    
}
