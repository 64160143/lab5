package com.lab5.week5;

public class BubbleSortApp {
    public static void main(String[] args) {
        int arr[] = new int[1000];
        randomArray(arr);
        print(arr);
        bubbleSort(arr);
        print(arr);
        
        
    }
    public static void print(int arr[]){
        for(int a:arr){
            System.out.println(a + " ");
        }
        System.out.println();
    }
    
    public static void randomArray(int arr[]){
        for(int i = 0; i<arr.length;i++){
            arr[i] = (int)(Math.random()*10000);
        } 
    }
    public static void swap(int[] arr, int fir, int sec) {
        int temp = arr[fir];
        arr[fir] = arr[sec];
        arr[sec] = temp;

    }

    public static void bubble(int[] arr, int fir, int sec) {
        for(int i = fir; i<sec; i++){
            if(arr[i]>arr[i+1]){
                swap(arr, i ,i+1);
            }
        }
    }

    public static void bubbleSort(int[] arr){
        for(int i = arr.length -1;i>0;i--){
            bubble(arr,0,i);
        }
    }



}
